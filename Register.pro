#-------------------------------------------------
#
# Project created by QtCreator 2014-12-27T22:47:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT += axcontainer

TARGET = Register
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui

RC_FILE = appicon.rc

#win32:RC_FILE += \
#    res/program.rc

OTHER_FILES += \
    res/program.rc \
    res/global/app.png \
    res/global/app.ico

RESOURCES += \
    res.qrc
