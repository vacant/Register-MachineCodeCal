#include "widget.h"
#include <QApplication>
#include <QFileInfo>
#include <QTranslator>
#include <QDebug>

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();
    }
}


void setupLang()
{
    QString langName = QLocale::system().name();
    QStringList list;
    list << ":/res/lang/qtbasesubset_%1.qm";
    foreach(QString fileName,list){
        QString langFile =  fileName.arg(langName);
        QFileInfo fileInfo(langFile);
        if(fileInfo.exists()){
            QTranslator *translator = new QTranslator(qApp);
            translator->load(langFile);
            qApp->installTranslator(translator);
        }
    }
}

int main(int argc, char *argv[])
{
    QString strQT_MESSAGE_PATTERN="[%{time yyyyMMdd h:mm:ss.zzz t} %{if-debug}D%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}] %{file}:%{line} - %{message}";
    qSetMessagePattern(strQT_MESSAGE_PATTERN);
    QApplication a(argc, argv);

    setupLang();

    Widget w;
    w.show();

    return a.exec();
}
