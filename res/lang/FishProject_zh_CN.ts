<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>FormSetting</name>
    <message>
        <location filename="../../ui/formsetting.ui" line="26"/>
        <source>设置面板</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="60"/>
        <source>组数设定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="66"/>
        <source>分组预览</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="100"/>
        <source>组号</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="105"/>
        <source>IP地址</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="110"/>
        <source>状态</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="145"/>
        <source>程序设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="165"/>
        <source>添加分组</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="178"/>
        <source>点击输入IP地址</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="191"/>
        <source>点击输入默认延时时间(分钟)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="207"/>
        <source>确认修改</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="223"/>
        <source>删除分组</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/formsetting.ui" line="230"/>
        <source>只允许删除最后一组</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../widget.ui" line="43"/>
        <source>连接</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="53"/>
        <source>断开连接</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="72"/>
        <source>导入数据</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="94"/>
        <source>设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="132"/>
        <source>分组</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="184"/>
        <source>跳转</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="201"/>
        <source>上一组</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="214"/>
        <source>下一组</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="232"/>
        <source>当前分组状态</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="250"/>
        <source>分组IP:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="263"/>
        <source>192.168.1.100</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="276"/>
        <source>分组状态:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="289"/>
        <source>未投料</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="302"/>
        <source>当前时间:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="315"/>
        <source>2014-08-17 21:50:5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="328"/>
        <source>预约放料时间:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="344"/>
        <source>2000-00-00 00:00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="367"/>
        <source>获取今日投料情况</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="374"/>
        <source>称料</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="403"/>
        <source>放料</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="499"/>
        <source>桶号</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="507"/>
        <source>称量(g)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.ui" line="515"/>
        <source>实际称量(g)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../widget.cpp" line="916"/>
        <source>投放记录.xls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
