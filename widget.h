#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void paintEvent(QPaintEvent */*event*/);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
private:
    Ui::Widget *ui;
    QPoint m_windowPos;
    QString m_currentBGColor;
    QString m_custRegistCode;

private:
    //function;
    QString getWMIHWInfo(int type);
    QString GetCustCodeOfFeeder();
    bool RegisterFeeder(QString appname);
    //test
    void paracode();

    QString calActiveCode(QString machineCode, int validays,QString username = "");
};

#endif // WIDGET_H
